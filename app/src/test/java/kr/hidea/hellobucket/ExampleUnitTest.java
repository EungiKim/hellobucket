package kr.hidea.hellobucket;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {


        String a = "0.6.6";
        String b = "0.6.8";
        String[] savedVersion = a.split("\\.");
        String[] currentVersion = b.split("\\.");

        System.out.println(Arrays.toString(savedVersion));

        int savedVersionMajor = Integer.parseInt(savedVersion[0]);
        int savedVersionMinor = Integer.parseInt(savedVersion[1]);
        int savedVersionPatch = Integer.parseInt(savedVersion[2]);
        int currentVersionMajor = Integer.parseInt(currentVersion[0]);
        int currentVersionMinor = Integer.parseInt(currentVersion[1]);
        int currentVersionPatch = Integer.parseInt(currentVersion[2]);

        assertEquals(4, 2 + 2);
    }
}