package kr.hidea.hellobucket.clicker.model;



import java.util.Optional;

import kr.hidea.hellobucket.clicker.viewmodel.ViewState;


public class ClickerModel implements Clicker {

	private int numberOfClicks;
	private Excitement stateOfExcitement;

	public void incrementClicks() {
		numberOfClicks += 1;
		updateStateOfExcitement();
	}

	public int getNumberOfClicks() {
		return Optional.ofNullable(numberOfClicks).orElse(0);
	}

	public Excitement getStateOfExcitement() {
		return Optional.ofNullable(stateOfExcitement).orElse(Excitement.BOO);
	}

	public void restoreState(ViewState state) {
		numberOfClicks = state.getNumberOfClicks();
		updateStateOfExcitement();
	}

	private void updateStateOfExcitement() {
		if (numberOfClicks < 10) {
			stateOfExcitement = Excitement.BOO;
		} else if (numberOfClicks <= 20) {
			stateOfExcitement = Excitement.MEH;
		} else {
			stateOfExcitement = Excitement.WOOHOO;
		}
	}
}
