package kr.hidea.hellobucket.clicker.view;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import kr.hidea.hellobucket.R;
import kr.hidea.hellobucket.clicker.api.ClickerController;
import kr.hidea.hellobucket.clicker.model.ClickerModel;
import kr.hidea.hellobucket.clicker.viewmodel.ViewState;
import kr.hidea.hellobucket.databinding.ActivityClickerBinding;
import kr.hidea.hellobucket.clicker.viewmodel.ClickerViewModel;
import rx.Subscription;
import rx.subscriptions.Subscriptions;

class ClickerActivity extends AppCompatActivity {

    private static final String KEY_VIEW_STATE = "state.view";

    private ClickerViewModel viewModel;
    private Subscription fakeLoader = Subscriptions.unsubscribed();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // would usually be injected but I feel Dagger would be out of scope
        final ClickerController api = new ClickerController();
        setupViewModel(savedInstanceState, api);

        ActivityClickerBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_clicker);
        binding.setViewModel(viewModel);
    }

    @Override
    protected void onPause() {
        fakeLoader.unsubscribe();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        fakeLoader.unsubscribe();
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(KEY_VIEW_STATE, viewModel.getViewState());
    }

    private void setupViewModel(Bundle savedInstance, ClickerController api) {
        viewModel = new ClickerViewModel(new ClickerModel(), api);
        final ViewState savedState = getViewStateFromBundle(savedInstance);

        if (savedState == null) {
            fakeLoader = viewModel.loadData().subscribe();
        } else {
            viewModel.initFromSavedState(savedState);
        }
    }

    private ViewState getViewStateFromBundle(Bundle savedInstance) {
        if (savedInstance != null) {
            return (ViewState) savedInstance.getSerializable(KEY_VIEW_STATE);
        }
        return null;
    }
}