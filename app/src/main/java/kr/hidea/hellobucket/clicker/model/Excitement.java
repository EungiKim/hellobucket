package kr.hidea.hellobucket.clicker.model;

public enum Excitement {
	BOO,
	MEH,
	WOOHOO
}