package kr.hidea.hellobucket.clicker.model;


import kr.hidea.hellobucket.clicker.viewmodel.ViewState;

public interface Clicker {

    int getNumberOfClicks();

    Excitement getStateOfExcitement();

    void incrementClicks();

    void restoreState(ViewState state);
}
