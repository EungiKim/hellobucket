package kr.hidea.hellobucket;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.ViewModel;

import java.time.LocalDateTime;

class FirstViewModel extends ViewModel {
    private SavedStateHandle mState;
    String test = mState.get("udi");
    LiveData<Object> user = (LiveData<Object>) new Object();
    LocalDateTime mLocalDateTime;

    public FirstViewModel(SavedStateHandle savedStateHandle) {
        mState = savedStateHandle;
        mLocalDateTime = LocalDateTime.now();
    }

}
